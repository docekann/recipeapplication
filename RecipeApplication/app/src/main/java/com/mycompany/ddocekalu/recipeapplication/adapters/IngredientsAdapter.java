package com.mycompany.ddocekalu.recipeapplication.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adastraone.ddocekalu.recipeapplication.R;
import com.mycompany.ddocekalu.recipeapplication.datastructures.Meal;
import com.mycompany.ddocekalu.recipeapplication.datastructures.MealCollection;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * An Adapter for {@link RecyclerView} that is able to display a {@link MealCollection}.
 */

public class IngredientsAdapter extends RecyclerView.Adapter<IngredientsAdapter.ViewHolder> {

    private Meal mealDataset;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ingredient_name) public TextView nameTextView;
        @BindView(R.id.dosage) public TextView dosageTextView;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public IngredientsAdapter(Meal mealDataset) {
        this.mealDataset = mealDataset;
    }

    public void setDataset(Meal mealDataset) {
        this.mealDataset = mealDataset;
        notifyDataSetChanged();
    }

    /**
     * Creates new views.
     * Invoked by layout manager.
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public IngredientsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ingredients_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.nameTextView.setText(mealDataset.getIngredients().get(position));
        holder.dosageTextView.setText(mealDataset.getMeasures().get(position));
    }

    /**
     * The method returns the size of the dataset (invoked by layout manager). Method assumes that object returned by
     * {@link Meal#getIngredients()} is of the same size as object returned by invoking {@link Meal#getMeasures()}.
     * If it is not so, an exception will probably occur during this adapter operation.
     *
     * @return size of dataset (that means primarily number of ingredients)
     */
    @Override
    public int getItemCount() {
        return mealDataset.getIngredients().size();
    }




}