package com.mycompany.ddocekalu.recipeapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.adastraone.ddocekalu.recipeapplication.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.view_latest_meals)
    protected void onButtonClick() {
        Intent intent = new Intent(this, MainActivity.class);
        this.startActivity(intent);
    }
}
