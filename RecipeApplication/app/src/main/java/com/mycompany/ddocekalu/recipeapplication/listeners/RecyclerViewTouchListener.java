package com.mycompany.ddocekalu.recipeapplication.listeners;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public abstract class RecyclerViewTouchListener implements RecyclerView.OnItemTouchListener {

    private GestureDetector gestureDetector;

    public RecyclerViewTouchListener(Context context) {

        gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    /**
     * Finds out if there is a item of recycler view, where the user touched.
     * If so, it redirects to perform custom action in {@link #onItemTouch(int)}
     * @param rv
     * @param e
     * @return true - the touched thing is a recycler view item
     */
    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        View childView = rv.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && gestureDetector.onTouchEvent(e)) {
            this.onItemTouch(rv.getChildAdapterPosition(childView));
            return true;
        }
        return false;
    }

    /**
     * Override this method to provide a custom action for touching an item at provided position in recycler view.
     *
     * @param childAdapterPosition position of data in the recycler view adapter
     */
    public abstract void onItemTouch(int childAdapterPosition);


    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}
