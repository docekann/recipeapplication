package com.mycompany.ddocekalu.recipeapplication;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.adastraone.ddocekalu.recipeapplication.R;
import com.mycompany.ddocekalu.recipeapplication.adapters.IngredientsAdapter;
import com.mycompany.ddocekalu.recipeapplication.adapters.InstructionsAdapter;
import com.mycompany.ddocekalu.recipeapplication.datastructures.Meal;
import com.mycompany.ddocekalu.recipeapplication.utils.StringFormattingUtils;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecipeDetailActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {

    private static final String TAG = RecipeDetailActivity.class.getSimpleName();


    @BindView(R.id.expanded_image) protected ImageView expandedImageView;
    @BindView(R.id.transparent_image) protected ImageView transparentImageView;
    @BindView(R.id.place) protected TextView placeTv;
    @BindView(R.id.category) protected TextView categoryTv;
    @BindView(R.id.tags) protected TextView tagsTv;
    @BindView(R.id.toolbar_layout) protected CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.app_bar) protected AppBarLayout appBarLayout;

    @BindView(R.id.ingredients_recycler_view) protected RecyclerView ingredientsRv;
    private IngredientsAdapter ingredientsAdapter;

    @BindView(R.id.instructions_recycler_view) protected RecyclerView instructionsRv;
    private InstructionsAdapter instructionsAdapter;

    @BindView(R.id.toolbar) protected Toolbar toolbar;
    @BindView(R.id.nested_scroll_view) protected NestedScrollView nestedScrollView;

    // constants related to transparency of one of the collapsing toolbar image
    private final int APP_BAR_IS_EXPANDED = 0;
    private static final int THRESHOLD = -100 ;

    // for tablets, there should be 2 columns
    private final int INGREDIENT_COLUMNS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_detail);
        ButterKnife.bind(this);

        appBarLayout.addOnOffsetChangedListener(this);

        ingredientsRv.setLayoutManager(new GridLayoutManager(this, INGREDIENT_COLUMNS));
        ingredientsRv.setFocusable(false);
        instructionsRv.setLayoutManager(new LinearLayoutManager(this));
        instructionsRv.setFocusable(false);
        // the screen should not start "in the middle" focusing on recycler views
        nestedScrollView.requestFocus();

        setSupportActionBar(toolbar);

        // receive extra from other ativity
        Bundle data = getIntent().getExtras();
        Meal meal =  data.getParcelable(MainActivity.EXTRA);
        Log.d(TAG, "received meal: " + meal.toString());

        updateGui(meal);
    }

    /**
     * Loads data onto the screen (images, texts).
     * @param meal
     */
    private void updateGui(Meal meal) {
        Picasso.with(this)
                .load(meal.getThumbUrl())
                .into(expandedImageView);

        // does not make sense that recipe title should be null..., but never say never... :D :D
        collapsingToolbarLayout.setTitle(meal.getName());

        updateView(placeTv, meal.getArea());
        updateView(categoryTv, meal.getCategory());

        String value = StringFormattingUtils.getCommaSeparatedString(meal.getTags());
        updateView(tagsTv, value);

        ingredientsAdapter = new IngredientsAdapter(meal);
        ingredientsRv.setAdapter(ingredientsAdapter);

        instructionsAdapter = new InstructionsAdapter(meal);
        instructionsRv.setAdapter(instructionsAdapter);

    }

    /**
     * Updates given text view according to the second parameter.
     * If the given {@link String} is null or empty, it makes the given text view disapear.
     * Otherwise, the text view will be visible and the given text set to it.
     * @param tv
     * @param text
     */
    private void updateView(TextView tv, String text){
        if (text == null || text.isEmpty()) {
            tv.setVisibility(View.GONE);
        } else {
            tv.setVisibility(View.VISIBLE);
            tv.setText(text);
        }
    }

    /**
     * Detects expansion or rolling up of the app bar and performs corresponding actions
     * @param appBarLayout
     * @param verticalOffset
     */
    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        Log.d(TAG, "vertical offset: " + verticalOffset);
        if (verticalOffset == APP_BAR_IS_EXPANDED || verticalOffset > THRESHOLD) {
            transparentImageView.setBackgroundColor(this.getResources().getColor(R.color.colorPrimaryTransparent));
        } else {
            transparentImageView.setBackgroundColor(this.getResources().getColor(R.color.colorPrimary));
        }
    }
}
