package com.mycompany.ddocekalu.recipeapplication.datastructures;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Data structure that corresponds to a meal on the server www.themealdb.com.
 * Example meal in json: https://www.themealdb.com/api/json/v1/1/lookup.php?i=52772
 *
 * @author d docekalu
 */

public class Meal implements Parcelable {

    private final String TAG = Meal.class.getSimpleName();

    private String id;
    private String name;
    private String category;
    private String area;
    private List<InstructionStep> instructions;

    private String thumbUrl;
    private String youtubeUrl;
    private String sourceUrl;

    private List<String> tags;
    private List<String> ingredients;
    private List<String> measures;

    private String dateModified;

    public Meal(String id, String name, String category, String area, List<InstructionStep> instructions,
                String thumbUrl, String youtubeUrl, String sourceUrl,
                List<String> tags, List<String> ingredients, List<String> measures,
                String dateModified) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.area = area;
        this.instructions = instructions;
        this.thumbUrl = thumbUrl;
        this.youtubeUrl = youtubeUrl;
        this.sourceUrl = sourceUrl;
        this.tags = tags;
        this.ingredients = ingredients;
        this.measures = measures;
        this.dateModified = dateModified;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public List<InstructionStep> getInstructions() {
        return instructions;
    }

    public void setInstructions(List<InstructionStep> instructions) {
        this.instructions = instructions;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public String getYoutubeUrl() {
        return youtubeUrl;
    }

    public void setYoutubeUrl(String youtubeUrl) {
        this.youtubeUrl = youtubeUrl;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public List<String> getMeasures() {
        return measures;
    }

    public void setMeasures(List<String> measures) {
        this.measures = measures;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    @Override
    public String toString() {
        return "Meal{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", area='" + area + '\'' +
                ", instructions='" + instructions + '\'' +
                ", thumbUrl='" + thumbUrl + '\'' +
                ", youtubeUrl='" + youtubeUrl + '\'' +
                ", sourceUrl='" + sourceUrl + '\'' +
                ", tags=" + tags +
                ", ingredients=" + ingredients +
                ", measures=" + measures +
                ", dateModified='" + dateModified + '\'' +
                '}';
    }

    // parcelable handling

    private Meal(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.category = in.readString();
        this.area = in.readString();

        List<InstructionStep> instructionSteps = new ArrayList<>();
        in.readTypedList(instructionSteps, InstructionStep.CREATOR);

        this.instructions = instructionSteps;
        this.thumbUrl = in.readString();
        this.youtubeUrl = in.readString();
        this.sourceUrl = in.readString();
        this.tags = (List<String>) in.readSerializable();
        this.ingredients = (List<String>) in.readSerializable();
        this.measures = (List<String>) in.readSerializable();
        this.dateModified = in.readString();

        Log.d(TAG, "parcelable read: " + instructionSteps.toString());
    }

    public static final Parcelable.Creator<Meal> CREATOR = new Parcelable.Creator<Meal>() {
        public Meal createFromParcel(Parcel in) {
            return new Meal(in);
        }

        public Meal[] newArray(int size) {
            return new Meal[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        Log.d(TAG, "parcelable to write: " + instructions.toString());

        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(category);
        parcel.writeString(area);
        parcel.writeTypedList(instructions);
        parcel.writeString(thumbUrl);
        parcel.writeString(youtubeUrl);
        parcel.writeString(sourceUrl);

        parcel.writeSerializable((Serializable) tags);
        parcel.writeSerializable((Serializable) ingredients);
        parcel.writeSerializable((Serializable)  measures);

        parcel.writeString(dateModified);
    }

}
