package com.mycompany.ddocekalu.recipeapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.adastraone.ddocekalu.recipeapplication.R;
import com.mycompany.ddocekalu.recipeapplication.adapters.MealCollectionAdapter;
import com.mycompany.ddocekalu.recipeapplication.datastructures.Meal;
import com.mycompany.ddocekalu.recipeapplication.datastructures.MealCollection;
import com.mycompany.ddocekalu.recipeapplication.listeners.RecyclerViewTouchListener;
import com.mycompany.ddocekalu.recipeapplication.networking.RESTClient;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    public static final String EXTRA = Meal.class.getSimpleName();

    @BindView(R.id.recycler_view) protected RecyclerView mRecyclerView;
    private MealCollectionAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private MealCollection mealCollection;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initializeGUI();

        downloadDataFromNetwork();
    }

    /**
     * Connection to the network, data reception and update of GUI accordingly ({@link #updateGUI(MealCollection)}),
     * or error handling.
     */
    private void downloadDataFromNetwork() {
        // networking
        RESTClient.getApiService(this).getLatestMeals().enqueue(
                new Callback<MealCollection>() {
                    @Override
                    public void onResponse(Call<MealCollection> call, Response<MealCollection> response) {
                        if (response.isSuccessful()) {
                            Log.d(TAG, "Response for latest meals successful.");
                            MealCollection mealCollection = response.body();

                            updateGUI(mealCollection);

                        } else {

                            //provide some error information
                            Log.e(TAG, "Response for latest meals not successful.");
                            String errMessage = "";
                            try {
                                errMessage = response.errorBody().string();
                                Log.e(TAG, errMessage);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            errMessage = getString(R.string.downloadNotSuccess).concat(errMessage);
                            Toast.makeText(MainActivity.this, errMessage, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<MealCollection> call, Throwable t) {
                        //provide failure information, most often, the network connection is not present
                        Log.e(TAG, "Failed to retrieve latest meals from the network.");
                        Log.e(TAG, t.getMessage());
                        t.printStackTrace();
                        Toast.makeText(MainActivity.this, getString(R.string.checkInternet), Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    /**
     * All the necessary initializations including setting up the adapters and listeners.
     */
    private void initializeGUI() {
        // set up recycler view
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mealCollection = new MealCollection();
        mAdapter = new MealCollectionAdapter(mealCollection, this);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(this) {
            @Override
            public void onItemTouch(int childAdapterPosition) {
                Meal meal = mAdapter.getItemAt(childAdapterPosition);
                Log.d(TAG, "touched at position: " + childAdapterPosition);
                Log.d(TAG, "touched meal with a name: " + meal.getName());
                // start detail activity
                Intent intent = new Intent(MainActivity.this, RecipeDetailActivity.class);
                intent.putExtra(MainActivity.EXTRA, meal);
                MainActivity.this.startActivity(intent);
            }
        });
        //divider between items
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));
    }

    /**
     * Updates activity gui if data change
     * @param mealCollection new data
     */
    private void updateGUI(MealCollection mealCollection) {
        mAdapter.setMealDataset(mealCollection);
    }

}


