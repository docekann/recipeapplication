package com.mycompany.ddocekalu.recipeapplication.networking;

import android.content.Context;
import android.util.Log;

import com.mycompany.ddocekalu.recipeapplication.datastructures.MealCollection;
import com.mycompany.ddocekalu.recipeapplication.deserialization.MealCollectionDeserializer;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;
import com.google.gson.GsonBuilder;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;

import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A singleton class that gives an access to {@link RESTService}
 *
 * @author d docekalu
 */

public class RESTClient {

    private static final String TAG = RESTClient.class.getSimpleName();
    /**
     * Api key for testing purposes.
     */
    private static final String API_KEY = "1";

    /**
     * Root URL for queries.
     */
    private static final String ROOT_URL =
            new StringBuilder("https://www.themealdb.com/api/json/v1/")
                    .append(API_KEY)
                    .append("/")
                    .toString();

    private static final int TIMEOUT_SECONDS = 20;

    private static Retrofit instance = null;

    private RESTClient(){

    }


    /**
     * Creates a {@link Retrofit} instance if it has not been created yet.
     * When creating the instance, it also updates android security managers and configures TLS with {@link OkHttpClient}
     *
     * @return {@link Retrofit} instance
     */
    private static Retrofit getRetrofitInstance(Context context) {
        if (instance != null) {
            return instance;
        }

        updateSecurityProvider(context);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        // set up connection timeouts and headers
        httpClient.addInterceptor(new SendHeadersInterceptor())
                .connectTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS);

        //solution with needed ciphers for 4.2+ devices
        //https://stackoverflow.com/questions/29916962/javax-net-ssl-sslhandshakeexception-javax-net-ssl-sslprotocolexception-ssl-han/36892715#36892715
        ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.COMPATIBLE_TLS)
                .supportsTlsExtensions(true)
                .tlsVersions(TlsVersion.TLS_1_2, TlsVersion.TLS_1_1, TlsVersion.TLS_1_0)
                .cipherSuites(
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA,
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_RC4_128_SHA,
                        CipherSuite.TLS_ECDHE_RSA_WITH_RC4_128_SHA,
                        CipherSuite.TLS_DHE_RSA_WITH_AES_128_CBC_SHA,
                        CipherSuite.TLS_DHE_DSS_WITH_AES_128_CBC_SHA,
                        CipherSuite.TLS_DHE_RSA_WITH_AES_256_CBC_SHA)
                .build();
        httpClient.connectionSpecs(Arrays.asList(spec));
        OkHttpClient client = httpClient.build();

        Log.d(TAG, ROOT_URL);

        instance =  new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                // register a custom deserializer from json
                .addConverterFactory(GsonConverterFactory.create(
                        new GsonBuilder()
                                .registerTypeAdapter(MealCollection.class, new MealCollectionDeserializer())
                                .create()
                ))
                .client(client)
                .build();

        return instance;
    }

    /**
     * Allow Android's security Provider to update. For solving problems with TLS on android API < 21.
     * Initializes SSL context.
     * Solution source: https://stackoverflow.com/questions/29916962/javax-net-ssl-sslhandshakeexception-javax-net-ssl-sslprotocolexception-ssl-han/36892715#36892715
     * @param context
     */
    private static void updateSecurityProvider(Context context) {
        if (android.os.Build.VERSION.SDK_INT < 21) {
            try {
                ProviderInstaller.installIfNeeded(context);
                Log.d(TAG, "on provider installing");
                SSLContext sslContext = null;

                sslContext = SSLContext.getInstance("TLSv1.2");
                sslContext.init(null, null, null);
                SSLEngine engine = sslContext.createSSLEngine();

                Log.d(TAG, "on provider installed and SSL context initialized");

            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Creates and returns a {@link RESTService} instance
     * @return a new {@link RESTService} object
     */
    public static RESTService getApiService(Context context) {
        return getRetrofitInstance(context).create(RESTService.class);
    }
}