package com.mycompany.ddocekalu.recipeapplication.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adastraone.ddocekalu.recipeapplication.R;
import com.mycompany.ddocekalu.recipeapplication.datastructures.Meal;
import com.mycompany.ddocekalu.recipeapplication.datastructures.MealCollection;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * An Adapter for {@link RecyclerView} that is able to display a {@link MealCollection}.
 */

public class InstructionsAdapter extends RecyclerView.Adapter<InstructionsAdapter.ViewHolder> {

    private Meal mealDataset;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.instruction_step) public TextView stepTextView;
        @BindView(R.id.instruction_step_text) public TextView textTextView;
        @BindView(R.id.instruction_step_title) public TextView titleTextView;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public InstructionsAdapter(Meal mealDataset) {
        this.mealDataset = mealDataset;
    }

    public void setDataset(Meal mealDataset) {
        this.mealDataset = mealDataset;
        notifyDataSetChanged();
    }

    /**
     * Creates new views.
     * Invoked by layout manager.
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public InstructionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.instructions_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.stepTextView.setText(""+(position+1));
        holder.titleTextView.setText(mealDataset.getInstructions().get(position).getTitle());
        holder.textTextView.setText(mealDataset.getInstructions().get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return mealDataset.getInstructions().size();
    }




}