package com.mycompany.ddocekalu.recipeapplication.utils;

import android.util.Log;

import com.mycompany.ddocekalu.recipeapplication.datastructures.InstructionStep;

import java.util.ArrayList;
import java.util.List;

/**
 * The class serves for transformations and formatting of objects of type {@link String}
 */
public class StringFormattingUtils {

    private final static String TAG = StringFormattingUtils.class.getSimpleName();
    private final static String COMMA_SEPARATOR = ", ";

    /**
     * Takes {@link List} of {@link String}
     * and transforms it into one {@link String} which contains comma separated ({@link #COMMA_SEPARATOR}) values of the original list.
     * @param list
     * @return empty string if the input parameter is null
     */
    public static String getCommaSeparatedString(List<String> list) {

        StringBuilder result = new StringBuilder();

        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                result.append(list.get(i));

                if (i < list.size() - 1) {
                    result.append(COMMA_SEPARATOR);
                }

            }
        }

        return result.toString();
    }

    /**
     * The method takes one string in format such as: <br>
     *  <br>
     * "STEP 1 - MARINATING THE CHICKEN\r\nIn a bowl, add chicken, salt, white pepper, ginger juice and then mix it together well.\r\nSet the chicken aside.\r\n <br>
     *  STEP 2 - RINSE THE WHITE RICE\r\nRinse the rice in a metal bowl or pot a couple times and then drain the water.\r\n <br>
     *  STEP 2 - BOILING THE WHITE RICE\r\nNext add 8 cups of water and then set the stove on high heat until it is boiling. <br>
     *  Once rice porridge starts to boil, set the stove on low heat and then stir it once every 8-10 minutes for around 20-25 minutes. <br>
     *  \r\nAfter 25 minutes, this is optional but you can add a little bit more water to make rice porridge to make it less thick or to your preference. <br>
     *  \r\nNext add the marinated chicken to the rice porridge and leave the stove on low heat for another 10 minutes. <br>
     *  \r\nAfter an additional 10 minutes add the green onions, sliced ginger, 1 pinch of salt, <br>
     *  1 pinch of white pepper and stir for 10 seconds.\r\nServe the rice porridge in a bowl\r\n <br>
     *  Optional: add Coriander on top of the rice porridge. <br>
     *   <br>
     *  The method divides the string into separate steps ({@link InstructionStep} objects) by the "STEP \\d - " substring. <br>
     *  The title of the instruction description will be the first line (ending by \r\n) after the "STEP \\d - " substring.
     *   <br>
     * @param original
     * @return if the input is an empty string or null the output is an empty list
     */
    public static List<InstructionStep> divideIntoInstructionSteps(String original) {

        String title = "";
        String stepDescription = "";
        List<InstructionStep> result = new ArrayList<>();

        if (original == null) return  result;

        String steps[] = original.trim().split("STEP\\ \\d\\ \\-\\ ");

        Log.d(TAG, "PARSED STEPS:");
        for (String step: steps) {
            String separatedStep[] = step.trim().split("\\r\\n");
            if (separatedStep.length > 1) {
                title = separatedStep[0];
                title = title.trim();
            }
            StringBuilder descBuilder = new StringBuilder();

            for (int i = 1; i < separatedStep.length; i++) {
                descBuilder.append(separatedStep[i]);
            }
            stepDescription = descBuilder.toString();

            Log.d(TAG,"T:|" + title + "|");
            Log.d(TAG,"S:|" + stepDescription + "|");

            if (title.isEmpty() && stepDescription.isEmpty()) {
                continue;
            } else {
                result.add(new InstructionStep(title, stepDescription));
            }

        }

        return result;
    }
}
