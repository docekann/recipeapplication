package com.mycompany.ddocekalu.recipeapplication.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adastraone.ddocekalu.recipeapplication.R;
import com.mycompany.ddocekalu.recipeapplication.datastructures.Meal;
import com.mycompany.ddocekalu.recipeapplication.datastructures.MealCollection;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * An Adapter for {@link RecyclerView} that is able to display a {@link MealCollection}.
 */

public class MealCollectionAdapter extends RecyclerView.Adapter<MealCollectionAdapter.ViewHolder> {
    private static final String UNKNOWN_INGREDIENTS = "unknown ingredients";
    private static final int SUBTITLE_INGREDIENTS_NUM = 5;
    private static final String INGREDIENT_SEPARATOR = ", ";
    private static final String AND_SO_ON = "...";

    private MealCollection mealDataset;
    private Context context;

    public Meal getItemAt(int childAdapterPosition) {
        return mealDataset.getMeal(childAdapterPosition);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.recipe_name) public TextView nameTextView;
        @BindView(R.id.dining_icon) public ImageView imageView;
        @BindView(R.id.some_ingredients) public TextView subtitle;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public MealCollectionAdapter(MealCollection mealDataset, Context context) {
        this.mealDataset = mealDataset;
        this.context = context;
    }

    public void setMealDataset(MealCollection mealDataset) {
        this.mealDataset = mealDataset;
        notifyDataSetChanged();
    }

    /**
     * Creates new views.
     * Invoked by layout manager.
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public MealCollectionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recipe_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Meal meal = mealDataset.getMeal(position);
        holder.nameTextView.setText(meal.getName());
        holder.subtitle.setText(getSomeIngredientsFormatted(meal.getIngredients()));
        Picasso.with(context)
                .load(meal.getThumbUrl())
                //https://www.flaticon.com/free-icon/meal_262343
                .error(R.drawable.error_thumb)
                .into(holder.imageView);
    }

    /**
     * Creates a comma separated string with up to five ingredients (depending how much ingredients is in the list).
     * If the input parametr is null or empty, it returns {@link #UNKNOWN_INGREDIENTS} constant.
     * @param ingredients
     * @return a string created from the input parameter
     */
    private String getSomeIngredientsFormatted (List<String> ingredients) {
        if (ingredients == null || ingredients.isEmpty()) {
            return UNKNOWN_INGREDIENTS;
        }

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < ingredients.size(); i++) {
            if (i == SUBTITLE_INGREDIENTS_NUM) break;
            builder.append(ingredients.get(i)).append(INGREDIENT_SEPARATOR);
        }
        builder.append(AND_SO_ON);

        return builder.toString();
    }

    /**
     * The method returns the size of the dataset.
     * Invoked by LayoutManager
     * @return size of dataset
     */
    @Override
    public int getItemCount() {
        return mealDataset.getNumberOfMeals();
    }




}