package com.mycompany.ddocekalu.recipeapplication.datastructures;

import java.util.ArrayList;
import java.util.List;

/**
 * A wrapper for a collection of {@link Meal} objects.
 */
public class MealCollection {
    private List<Meal> mealCollection;

    public MealCollection() {
        this.mealCollection = new ArrayList<>();
    }

    public boolean isEmpty(){
        return mealCollection == null || mealCollection.isEmpty();
    }

    /**
     * Gets the number of meals present in the collection.
     * @return an integer representing number of meals.
     */
    public int getNumberOfMeals(){
        if (!isEmpty()){
            return mealCollection.size();
        }
        return 0;
    }

    /**
     * Gets a {@link Meal} at specified index in the collection or null if the collection is empty.
     * @param idx index of a meal
     * @return {@link Meal} object or null
     */
    public Meal getMeal(int idx) {
        if (!isEmpty()) {
            return mealCollection.get(idx);
        }
        return null;
    }

    /**
     * Adds a meal to the next free index in the collection.
     * @param meal {@link Meal} instance
     */
    public void addMeal(Meal meal) {
        mealCollection.add(meal);
    }

}
