package com.mycompany.ddocekalu.recipeapplication.deserialization;

import android.util.Log;

import com.mycompany.ddocekalu.recipeapplication.datastructures.InstructionStep;
import com.mycompany.ddocekalu.recipeapplication.datastructures.Meal;
import com.mycompany.ddocekalu.recipeapplication.datastructures.MealCollection;
import com.mycompany.ddocekalu.recipeapplication.utils.StringFormattingUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A custom deserializer of class {@link MealCollection} from JSON
 */
public class MealCollectionDeserializer implements JsonDeserializer<MealCollection> {

    private static final String TAG = MealCollectionDeserializer.class.getSimpleName();

    @Override
    public MealCollection deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        MealCollection collection =  new MealCollection();
        JsonArray jsonArray = json.getAsJsonObject().getAsJsonArray("meals");

        for (JsonElement elem: jsonArray) {
            JsonObject object = elem.getAsJsonObject();

            String id = getStringFromJsonObject(object,"idMeal");
            String name = getStringFromJsonObject(object,"strMeal");
            String category = getStringFromJsonObject(object,"strCategory");
            String area = getStringFromJsonObject(object,"strArea");
            String instructions = getStringFromJsonObject(object,"strInstructions");

            List<InstructionStep> steps = StringFormattingUtils.divideIntoInstructionSteps(instructions);

            String thumb = getStringFromJsonObject(object,"strMealThumb");
            String video = getStringFromJsonObject(object,"strYoutube");
            String source = getStringFromJsonObject(object,"strSource");

            String date = getStringFromJsonObject(object,"dateModified");

            List<String> ingredients = getListFromJsonObject(object, "strIngredient");
            List<String> measures = getListFromJsonObject(object, "strMeasure");

            String tags = getStringFromJsonObject(object, "strTags"); // returns comma separated values
            List<String> tagList = null;

            if (tags != null) {
                tagList = Arrays.asList(tags.split(","));
            }

            Meal meal = new Meal(id, name, category, area, steps,
                    thumb, video, source,
                    tagList, ingredients, measures, date);

            collection.addMeal(meal);
        }

        return collection;
    }

    /**
     * Returns a property from jsonObject by it's name or null.
     * @param jsonObject
     * @param name name of the property to be retrieved from jsonObject
     * @return a string or null if {@link JsonElement#isJsonNull} or  {@link String#isEmpty()} evaluates as true
     */
    private String getStringFromJsonObject(JsonObject jsonObject, String name) {
        if (jsonObject.get(name) == null || jsonObject.get(name).isJsonNull()) {
            Log.d(TAG, name + "property is null");
            return null;
        } else {
            String str = jsonObject.get(name).getAsString();
            if (str.isEmpty()) {
                return null;
            }
            Log.d(TAG, name + "property:" + str);
            return str;
        }
    }

    /**
     *  Returns a list of property values which have the same base propertyName
     *  (e.g. strIngredient1, strIngredient2, ...)
     * @param jsonObject
     * @param propertyName a base name for a property, the name is further concatenated with consecutive integer values (i = 1, 2, ...)
     * @return a {@link List} of {@String} values or null if the list should be empty
     */
    private List<String> getListFromJsonObject(JsonObject jsonObject, String propertyName) {

        List<String> list = new ArrayList<>();

        int i = 1;
        while (true) {
            String str = getStringFromJsonObject(jsonObject, propertyName + i);
            if (str == null){
                break;
            }
            list.add(str);
            i++; // the maximum index i saw on the server is 20
        }

        if (list.isEmpty()) {
            return null;
        }

        return list;
    }
}
