package com.mycompany.ddocekalu.recipeapplication.networking;

import com.mycompany.ddocekalu.recipeapplication.datastructures.MealCollection;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RESTService {

    /**
     * Gets a collection of latest meals
     * @return {@link MealCollection}
     */
    @GET("latest.php")
    Call<MealCollection> getLatestMeals();
}
