package com.mycompany.ddocekalu.recipeapplication.networking;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * The class adds headers to the requests (Accept, Accept-Language, Accept-Encoding, User-Agent)
 * before the request is sent to the server.
 * @author d docekalu
 */

public class SendHeadersInterceptor implements Interceptor {
    private static final String TAG = SendHeadersInterceptor.class.getSimpleName();

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        Request.Builder requestBuilder = original.newBuilder()
                .header("Accept", "application/json")
                .header("Accept-Language", "en")
                .header("Accept-Encoding", "utf-8")
                .header("User-Agent", "Recipe Application");

        Request request = requestBuilder
                .method(original.method(), original.body())
                .build();

        return chain.proceed(request);
    }
}
