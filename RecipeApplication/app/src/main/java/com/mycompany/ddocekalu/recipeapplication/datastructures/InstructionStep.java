package com.mycompany.ddocekalu.recipeapplication.datastructures;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * A class that represents one instruction step with a title and a description of the step.
 */
public class InstructionStep implements Parcelable {
    private String title;
    private String description;

    public InstructionStep(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "InstructionStep{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    // parcelable handling
    private InstructionStep(Parcel in) {
        this(in.readString(), in.readString());
    }

    public static final Parcelable.Creator<InstructionStep> CREATOR = new Creator<InstructionStep>() {
        public InstructionStep createFromParcel(Parcel in) {
            return new InstructionStep(in);
        }

        public InstructionStep[] newArray(int size) {
            return new InstructionStep[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(description);
    }
}
